import { Meteor } from 'meteor/meteor';
import { Categories, Inputs, CategoryItems } from '../../../lib/collections';

Meteor.publish('Categories', function categoryPublication() {
  return Categories.find();
});

Meteor.publish('Inputs', function categoryPublication() {
  return Inputs.find();
});

Meteor.publish('CategoryItems', function categoryPublication() {
  return CategoryItems.find();
});

