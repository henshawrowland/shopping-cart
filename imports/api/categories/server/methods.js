import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Categories, Inputs, CategoryItems } from '../../../lib/collections';

const methods = {
  'categories/insert': function (categoryData) {
    const { categoryName } = categoryData;
    const found = Categories.findOne({
      categoryName,
    });

    if (!found) {
      return Categories.insert({ ...categoryData, createdAt: new Date() });
    }
    throw new Meteor.Error('Category Name is taken');
  },

  'categories/delete': function (categoryId) {
    check(categoryId, String);

    // TODO, remove associated inputs
    Inputs.remove({ categoryId });
    return Categories.remove(categoryId);
  },
  
  'categories/discount/add': function (categoryId, discount) {
    check(categoryId, String);

    const selector = {
      _id: categoryId,
    };

    const update = {
      $addToSet: {
        discounts: discount,
      },
    };

    return Categories.update(selector, update);
  },

  'categories/discount/delete': function (categoryId, discountId) {
    check(categoryId, String);
    check(discountId, String);

    const category = Categories.findOne(categoryId);

    const filteredDiscounts = category.discounts.filter(({ _id }) => _id !== discountId);

    return Categories.update({
      _id: categoryId,
    }, {
      $set: {
        discounts: filteredDiscounts,
      },
    });
  },

  'categories/discount/update': function (categoryId, discount) {
    check(categoryId, String);

    const discountId = discount._id;

    const category = Categories.findOne(categoryId);

    const mappedDiscounts = category.discounts.map((item) => {
      if (item._id === discountId) {
        return discount;
      }
      return item;
    });

    return Categories.update({
      _id: categoryId,
    }, {
      $set: {
        discounts: mappedDiscounts,
      },
    });
  },

  'categories/input/add': function (categoryId, name) {
    check(categoryId, String);
    check(name, String);

    // const selector = {
    //   _id: categoryId,
    // };

    // const update = {
    //   $addToSet: {
    //     inputFields: input,
    //   },
    // };
    const input = {
      categoryId,
      name,
    };

    Inputs.insert(input);
  },

  'categories/input/update': function (inputId, data) {
    check(inputId, String);

    return Inputs.update({
      _id: inputId,
    }, {
      $set: {
        ...data,
      },
    });
  },

  'categoryItem/save': function (item) {
    CategoryItems.insert(item);
  },

  // 'get/inputList': function () {
  //   const inputs = Inputs.find({}).fetch();
  //   return inputs.map((item) => {
  //     const { categoryName } = item.category();
  //     return {
  //       ...item,
  //       displayName: `${categoryName}_${item.name}`.trim(),
  //       // eslint-disable-next-line no-useless-concat
  //       reference: `${'${' + `${categoryName}_${item.name}`}${'}'}`,
  //     };
  //   });
  // },
};

Meteor.methods(methods);
