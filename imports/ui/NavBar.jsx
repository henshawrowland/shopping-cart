import React from 'react';
import { Nav } from 'react-bootstrap';

const NavBar = () => (
  <Nav variant="tabs" defaultActiveKey="/">
    <Nav.Item>
      <Nav.Link href="/">Catalogue</Nav.Link>
    </Nav.Item>
    <Nav.Item>
      <Nav.Link eventKey="link-1">Cart</Nav.Link>
    </Nav.Item>
    <Nav.Item>
      <Nav.Link eventKey="disabled" disabled>
        Order Preview
      </Nav.Link>
    </Nav.Item>
  </Nav>
);

export default NavBar;
