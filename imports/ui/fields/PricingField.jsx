/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable react/prop-types */
import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import {
  TextField,
} from 'uniforms-bootstrap4';
import {
  Button,
  ButtonGroup,
  Form,
} from 'react-bootstrap';

import { Inputs } from '../../lib/collections';

const PricingField = ({
  inputFields,
  itemRef,
  setState,
  state,
}) => {
  const setPricingFormula = (value) => {
    const model = itemRef.getModel();
    setState({
      ...state,
      model: {
        ...model,
        pricingFormula: value,
      },
    });
  };
  const handleChange = (value) => {
    setPricingFormula(value);
    itemRef.validate('pricingFormula', value);
  };
  const setVariable = (item) => {
    const formula = state.model.pricingFormula + item.reference;
    setPricingFormula(formula);
    itemRef.validate('pricingFormula', formula);
  };

  return (
    <Form.Group widths="equal">
      <TextField
        label=""
        name="pricingFormula"
        showInlineError
        placeholder="Pricing Formula"
        onChange={handleChange}
        value={state.model.pricingFormula}
        list="variable-list"
        type="input"
      />
      <p>The following input items are available</p>
      <ButtonGroup className="pricing-formula-group-btn">
        {inputFields.map((item) => (
          <Button
            variant="secondary"
            className="pricing-formula-btn"
            key={item._id}
            onClick={() => { setVariable(item); }}
          >
            {item.displayName.trim()}
          </Button>
        ))}
      </ButtonGroup>
    </Form.Group>
  );
};

export default withTracker(() => {
  const inputsHandle = Meteor.subscribe('Inputs');
  const loading = !inputsHandle.ready();
  const inputs = Inputs.find({}).fetch();

  let items = [];

  if (!loading) {
    items = inputs.map((item) => {
      const { categoryName } = item.category();
      return {
        ...item,
        displayName: `${categoryName}_${item.name}`.toLowerCase().trim(),
        // eslint-disable-next-line no-useless-concat
        reference: `${'${' + `${categoryName}_${item.name}`}${'}'}`,
      };
    });
  }

  return {
    loading,
    inputFields: items,
  };
})(PricingField);
