/* eslint-disable react/prop-types */
import React from 'react';
import { Meteor } from 'meteor/meteor';
import SimpleSchema2Bridge from 'uniforms-bridge-simple-schema-2';
import swal from 'sweetalert';
import {
  AutoForm,
  TextField,
  SubmitField,
} from 'uniforms-bootstrap4';
import {
  Card,
  Container,
  Row,
  Col,
  Form,
} from 'react-bootstrap';
import { Category } from '../../lib/schemas';

const schema = new SimpleSchema2Bridge(Category);

const CategoryForm = ({
  model,
  categoryRef,
  setFormRef,
  removeCategory,
  index,
}) => {
  const saveCategory = () => {
    const { categoryName } = categoryRef.getModel();
    Meteor.call('categories/insert', { categoryName }, (e) => {
      if (e && e.error) {
        swal('Error', e.error, 'error');
      } else {
        // this code will clear all the value in the form.
        // ref.reset();
        // Necessary code after success is written here.
        
      }
    });
  };

  const deleteCategory = () => {
    const categoryId = categoryRef.getModel()._id;
    if (!categoryId) {
      removeCategory(index);
    } else {
      Meteor.call('categories/delete', categoryId, (e) => {
        if (e && e.error) {
          swal('Error', e.error, 'error');
          // Necessary code for handling error is written here.
        } else {
          // do something
          
        }
      });
    }
  };

  return (
    <AutoForm
      schema={schema}
      onSubmit={saveCategory}
      model={model}
      ref={(ref) => {
        setFormRef(ref);
      }}
    >
      <Form.Group>
        <Card.Body>
          <TextField
            name="categoryName"
            showInlineError
            placeholder="Enter Category Name"
            disabled={!!model._id}
          />
        </Card.Body>
      </Form.Group>
      <Container>
        <Row>
          <Col sm={10}>
            <SubmitField
              style={{ padding: '1.25rem' }}
              inputClassName="btn btn-success"
              disabled={!!model._id}
              value="Add New"
            />
          </Col>
          <Col sm={2}>
            <SubmitField
              style={{ padding: '1.25rem' }}
              inputClassName="btn btn-danger"
              onClick={(e) => {
                e.preventDefault();
                deleteCategory();
              }}
              value="Delete Category"
            />
          </Col>
        </Row>
      </Container>
    </AutoForm>
  );
};

export default CategoryForm;
