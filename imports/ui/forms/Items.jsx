/* eslint-disable react/prop-types */
import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import SimpleSchema2Bridge from 'uniforms-bridge-simple-schema-2';
import swal from 'sweetalert';

import {
  TextField,
  SelectField,
  AutoForm,
  SubmitField,
} from 'uniforms-bootstrap4';
import {
  Button,
  Form,
  Modal,
} from 'react-bootstrap';

import { CategoryItems } from '../../lib/collections';
import { CategoryItem } from '../../lib/schemas';

import PricingField from '../fields/PricingField';
import Table from '../components/Table';
import icons from '../components/Icons';

const schema = new SimpleSchema2Bridge(CategoryItem);

export const ItemForm = ({
  state,
  itemRef,
  setRef,
  setState,
  categories,
  handleShow,
}) => {
  const handleClose = () => {
    setState({
      ...state,
      showItemForm: false,
      model: {
        ...state.model,
        name: '',
        interval: '',
        pricingFormula: '',
      },
    });
  };

  const saveItem = () => {
    const item = itemRef.getModel();
    Meteor.call('categoryItem/save', item, (e) => {
      if (e && e.error) {
        console.log('ERROR SAVING CATEGORY ITEM', e);
        swal('Error', e.error, 'error');
      } else {
        handleClose();
        swal('Success', 'Item saved');
      }
    });
  };

  return (
    <>
      <Button
        variant="primary"
        size="lg"
        onClick={handleShow}
        block
        disabled={!state.model.categoryId}
      >
        Add Item
      </Button>
      <Modal show={state.showItemForm} onHide={handleClose}>
        <AutoForm
          schema={schema}
          onSubmit={saveItem}
          model={state.model}
          ref={(ref) => {
            setRef(ref);
          }}
          validate="onChange"
        >
          <Modal.Header closeButton>
            <Modal.Title>Insert Item</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group widths="equal" className="category-item-interval">
              <TextField
                label=""
                name="name"
                placeholder="Name"
                showInlineError
              />
              <Form.Group widths="equal">
                <SelectField
                  name="interval"
                  inline
                  checkboxes
                  showInlineError
                />
              </Form.Group>
              <PricingField
                categories={categories}
                itemRef={itemRef}
                setState={setState}
                state={state}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <SubmitField value="Save Changes" />
          </Modal.Footer>
        </AutoForm>
      </Modal>
    </>
  );
};

const List = ({ itemData, editInputItem, deleteInputItem }) => {
  if (!itemData.length) return null;
  const itemWithActionButtons = itemData.map((item) => ({
    ...item,
    tdClass: 2,
    actions: (
      <>
        <Button onClick={() => editInputItem(item)} className="action-btn" variant="light" style={{ marginRight: '0.25rem' }}>
          {icons.edit}
        </Button>
        <Button onClick={() => deleteInputItem(item)} className="action-btn" variant="light">
          {icons.delete}
        </Button>
      </>
    ),
  }));
  const tableData = {
    columns: [
      { key: 'name' },
      { key: 'pricingFormula' },
      { key: 'interval' },
      { key: 'actions', hide: true, trStyles: { width: '15%', padding: '0.25rem' } },
    ],
    rows: itemWithActionButtons,
  };

  return (
    <Table
      {...tableData}
    />
  );
};

export const ItemTable = withTracker(({ categoryId }) => {
  const itemsHandle = Meteor.subscribe('CategoryItems');
  const loading = !itemsHandle.ready();
  const items = CategoryItems.find({ categoryId }).fetch({});
  return {
    loading,
    itemData: items || [],
  };
})(List);
