/* eslint-disable react/prop-types */
import React from 'react';
import { Meteor } from 'meteor/meteor';
import SimpleSchema2Bridge from 'uniforms-bridge-simple-schema-2';
import { withTracker } from 'meteor/react-meteor-data';
import {
  TextField,
  AutoForm,
} from 'uniforms-bootstrap4';
import {
  Button,
  Form,
  Card,
} from 'react-bootstrap';

import { Inputs } from '../../lib/collections';
import { InputItem } from '../../lib/schemas';

import Table from '../components/Table';
import icons from '../components/Icons';

const schema = new SimpleSchema2Bridge(InputItem);

export const InputForm = ({
  state,
  inputRef,
  setRef,
  setState,
}) => {
  const addItem = () => {
    const { categoryId, name } = inputRef.getModel();
    Meteor.call('categories/input/add', categoryId, name, (err) => {
      if (err) {
        console.log('ERROR ADDING INPUT', err.error);
      } else {
        inputRef.reset();
      }
    });
  };

  const updateItem = () => {
    const { _id: inputId, name } = inputRef.getModel();

    Meteor.call('categories/input/update', inputId, { name }, (err) => {
      if (err) {
        console.log('ERROR UPDATING INPUT', err);
      } else {
        setState({
          ...state,
          isEditing: false,
          model: {
            ...state.model,
            name: '',
          },
        });
      }
    });
  };

  if (!state.model.categoryId) return null;

  return (
    <AutoForm
      schema={schema}
      onSubmit={addItem}
      model={state.model}
      ref={(ref) => {
        setRef(ref);
      }}
    >
      {state.isEditing ? (
        <Button variant="primary" size="lg" onClick={updateItem} block>
          Save Changes
        </Button>
      ) : (
        <Button
          variant="primary"
          size="lg"
          onClick={(e) => {
            e.preventDefault();
            inputRef.submit().then(console.log);
          }}
          block
        >
          Add Input
        </Button>
      )}
      <Form.Group>
        <Card.Body>
          <TextField
            label=""
            name="name"
            showInlineError
            placeholder="Input Name"
          />
        </Card.Body>
      </Form.Group>
    </AutoForm>
  );
};

const List = ({
  inputData,
  setState,
  state,
}) => {
  const editItem = (item) => {
    setState({
      ...state,
      isEditing: true,
      model: item,
    });
  };

  const deleteItem = (item) => {
    const inputId = item._id;
    // TODO: prompt are you sure ?

    Meteor.call('categories/input/delete', inputId, (err) => {
      if (err) {
        console.log('ERROR DELETING INPUT', err);
        // Necessary code for handling error is written here.
      } else {
        // do something
      }
    });
  };

  if (!inputData.length) return null;
  const inputWithActionButtons = inputData.map((item) => ({
    ...item,
    tdClass: 2,
    actions: (
      <>
        <Button onClick={() => editItem(item)} className="action-btn" variant="light" style={{ marginRight: '0.25rem' }}>
          {icons.edit}
        </Button>
        <Button onClick={() => deleteItem(item)} className="action-btn" variant="light">
          {icons.delete}
        </Button>
      </>
    ),
  }));
  const tableData = {
    columns: [{ key: 'name' }, { key: 'actions', hide: true, trStyles: { width: '15%', padding: '0.25rem' } }],
    rows: inputWithActionButtons,
  };

  return (
    <Table
      {...tableData}
    />
  );
};

export const InputTable = withTracker(({ categoryId }) => {
  const inputsHandle = Meteor.subscribe('Inputs');
  const loading = !inputsHandle.ready();
  const inputs = Inputs.find({ categoryId }).fetch();
  return {
    loading,
    inputData: inputs || [],
  };
})(List);
