import React from 'react';
import { Router, Route, Switch } from 'react-router';
import { createBrowserHistory } from 'history';

// route components
import CatalogueConatiner from './pages/Catalogue/container';
import ShoppingCart from './pages/ShoppingCart';
import OrderPreview from './pages/OrderPreview';

// import NotFoundPage from '../../ui/pages/NotFoundPage.js';

const browserHistory = createBrowserHistory();

const Routes = () => (
  <Router history={browserHistory}>
    <Switch>
      <Route exact path="/" component={CatalogueConatiner} />
      <Route exact path="/cart" component={ShoppingCart}/>
      <Route exact path="/preview" component={OrderPreview}/>
      {/*<Route component={NotFoundPage}/>*/}
    </Switch>
  </Router>
);

export default Routes;