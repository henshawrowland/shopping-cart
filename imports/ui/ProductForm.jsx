import React from "react";
import { Meteor } from "meteor/meteor";
import { AutoForm, TextField, LongTextField, SelectField, NumField, SubmitField  } from "uniforms-bootstrap4";
import { Form } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import { Schema as ProductSchema } from "../api/products";

const ProductForm = ({ model = undefined }) => {
  let formRef;
  const handleSubmit = (data, formRef) => {
    console.log('daata', data);
    console.log('formRef', formRef);
    Meteor.call("products.insert", data, err => {
      console.log('daata', data);
      if (err) {
        console.log(err);
        // Necessary code for handling error is written here.
        return;
      } else {
        // this code will clear all the value in the form.
        formRef.clear;
        // Necessary code after success is written here.
        return;
      }
    });
  }

    return (
      <AutoForm
        schema={ProductSchema}
        ref={ref => {
          formRef = ref;
        }}
        onSubmit={data => {
          console.log('dart', formRef);
          handleSubmit(data, formRef);
        }}
      >
          <Form.Group>
            <TextField
              name={"productName"}
              showInlineError={true}
              placeholder="Enter Product Name"
            />
            <TextField
              name={"price"}
              showInlineError={true}
              placeholder="Enter Product Price"
            />
          </Form.Group>
          <Form.Group widths="equal">
            <TextField
              name={"color"}
              showInlineError={true}
              placeholder="Enter Product color"
            />
            <TextField
              name={"brand"}
              showInlineError={true}
              placeholder="Enter Product brand"
            />
          </Form.Group>
          <Form.Group widths="equal">
            <LongTextField
              name={"productDescription"}
              showInlineError={true}
              placeholder="Enter Product Description"
            />
          </Form.Group>
          <Form.Group widths="equal">
            <SelectField name={"productCategory"} showInlineError={true} />
            <TextField
              name={"productYear"}
              showInlineError={true}
              placeholder="Enter manufactured year"
            />
          </Form.Group>
          <Form.Group widths="equal">
            <LongTextField
              name={"conditions"}
              showInlineError={true}
              placeholder="Product Condition"
            />
          </Form.Group>
          <Form.Group widths="equal">
            <NumField name={"userFor"} showInlineError={true} />
            <SelectField name={"homeDeliveryOptions"} showInlineError={true} />
          </Form.Group>
        <SubmitField onClick={() => formRef.submit()} />
      </AutoForm>
    );
}


export default ProductForm;
