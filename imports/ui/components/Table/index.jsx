/* eslint-disable react/no-array-index-key */
/* eslint-disable react/prop-types */
import React from 'react';
import {
  Table,
} from 'react-bootstrap';

const CustomTable = ({ columns, rows }) => (
  <Table striped bordered hover>
    <thead>
      <tr>
        {columns.map(({ key, hide = false }, index) => <th key={index}>{hide ? '' : key}</th>)}
      </tr>
    </thead>
    <tbody>
      {rows.map((item, i) => (
        <tr key={i}>
          {columns.map(({ key, trStyles }, j) => (
            <td style={trStyles} key={j}>{item[key]}</td>
          ))}
        </tr>
      ))}
    </tbody>
  </Table>
);

export default CustomTable;
