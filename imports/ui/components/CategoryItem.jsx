/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import {
  Accordion,
  Card,
  Button,
  Container,
  Row,
  Col,
  Tabs,
  Tab,
} from 'react-bootstrap';

import '/client/category-item.less';

import icons from './Icons';
import { InputForm, InputTable } from '../forms/Inputs';
import { ItemForm, ItemTable } from '../forms/Items';
import CategoryForm from '../forms/Category';

const Categoryitem = ({ item, index, removeCategory }) => {
  const [isClosed, setToggleState] = useState(true);
  const {
    categoryName,
    _id,
  } = item;
  const model = {
    categoryName,
    _id,
  };
  const [categoryRef, setCategoryFormRef] = useState(null);
  const [inputRef, setInputFormRef] = useState(null);
  const [itemRef, setItemRef] = useState(null);

  const [inputState, setInputState] = useState({
    isEditing: false,
    disabled: false,
    model: {
      categoryId: model._id,
      name: '',
      value: '',
    },
  });

  const [itemState, setItemState] = useState({
    isEditing: false,
    disabled: false,
    showItemForm: false,
    model: {
      categoryId: model._id,
      name: '',
      interval: '',
      pricingFormula: '',
    },
  });

  const demoteItem = (e) => {
    e.stopPropagation();
    console.log('item', item);
  };

  const promoteItem = (e) => {
    e.stopPropagation();
    console.log('item', item);
  };

  const handleToggleState = () => {
    setToggleState(!isClosed);
  };

  return (
    <>
      <Accordion>
        <Card>
          <Accordion.Toggle
            as={Card.Header}
            variant="link"
            eventKey="0"
            onClick={(e) => handleToggleState}
          >
            <Container>
              <Row>
                <Col sm={10}>
                  <h3>
                    {categoryName}
                  </h3>
                </Col>
                <Col sm={2}>
                  <Button onClick={(e) => demoteItem(e)} className="action-btn" variant="light">
                    {icons['arrow-down']}
                  </Button>
                  <Button onClick={(e) => promoteItem(e)} className="action-btn" variant="light">
                    {icons['arrow-up']}
                  </Button>
                  <Button className="action-btn" variant="light">
                    {isClosed ? icons['chevy-closed'] : icons['chevy-open']}
                  </Button>
                </Col>
              </Row>
            </Container>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              <Tabs defaultActiveKey="home" id="uncontrolled-tab-example">
                <Tab eventKey="home" title="Texts">
                  <CategoryForm
                    model={model}
                    setFormRef={setCategoryFormRef}
                    categoryRef={categoryRef}
                    removeCategory={removeCategory}
                    index={index}
                  />
                </Tab>
                <Tab eventKey="profile" title="Inputs">
                  <InputForm
                    state={inputState}
                    setRef={setInputFormRef}
                    inputRef={inputRef}
                    setState={setInputState}
                  />
                  <InputTable
                    categoryId={inputState.model.categoryId}
                    setState={setInputState}
                    state={inputState}
                  />
                </Tab>
              </Tabs>
              <fieldset>
                <legend>Item List</legend>
                <ItemForm
                  handleShow={() => setItemState({ ...itemState, showItemForm: true })}
                  state={itemState}
                  itemRef={itemRef}
                  setState={setItemState}
                  setRef={setItemRef}
                />
                <ItemTable
                  editDiscountItem={() => {}}
                  deleteDiscountItem={() => {}}
                  categoryId={itemState.model.categoryId}
                />
              </fieldset>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </>
  );
};

export default Categoryitem;
