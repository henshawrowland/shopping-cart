import React from 'react';
import Routes from './Routes';
import NavBar from './NavBar';

import 'bootstrap/dist/css/bootstrap.min.css';

// eslint-disable-next-line import/prefer-default-export
export const App = () => (
  <div>
    <NavBar />
    <Routes />
  </div>
);
