import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import { Categories } from '../../../lib/collections';
import CataloguePage from '.';

const CataloguePageContainer = withTracker(() => {
  const categoriesHandle = Meteor.subscribe('Categories');
  const loading = !categoriesHandle.ready();
  const categories = Categories.find({}, { sort: { createdAt: -1 } }).fetch();
  return {
    loading,
    data: categories || [],
  };
})(CataloguePage);

export default CataloguePageContainer;
