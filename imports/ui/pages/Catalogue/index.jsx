/* eslint-disable react/prop-types */
import React, { useState, useRef, useEffect } from 'react';
import { Button } from 'react-bootstrap';

import CategoryItem from '../../components/CategoryItem';

const ProductForm = ({ data, loading }) => {
  const [categories, setCategories] = useState(data);

  const isInitialMount = useRef(true);

  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      setCategories(data);
    }
  }, [data]);

  const addCategory = () => {
    const categoryModel = {
      categoryName: `New Category ${categories.length - 1}`,
    };

    setCategories([categoryModel, ...categories]);
  };

  const removeCategory = (index) => {
    const filteredCategories = categories.filter((_, i) => i !== index);
    setCategories(filteredCategories);
  };

  if (loading) return null;

  return (
    <>
      <h1>Manage Categories and Items</h1>
      <Button variant="primary" size="lg" block onClick={addCategory}>Add Category</Button>
      {categories.map((item, index) => <CategoryItem key={item._id || `${index}-${item.categoryName}`} item={item} index={index} removeCategory={removeCategory} />)}
    </>
  );
};

export default ProductForm;
