/* eslint-disable import/no-cycle */
import { Inputs } from '../../collections';
import Calculation from '../../calculation';

function categoryItemValidator() {
  if (this.key === 'pricingFormula') {
    if (Number(this.value)) {
      return undefined;
    }
    
    const inputs = Inputs.find({}).fetch();
    const inputList = inputs.map((item) => {
      const { categoryName } = item.category();
      return {
        ...item,
        displayName: `${categoryName}_${item.name}`.trim(),
        // eslint-disable-next-line no-useless-concat
        reference: `${'${' + `${categoryName}_${item.name}`}${'}'}`,
      };
    });
    const isValid = Calculation.isValidFormula(this.value, inputList);
    if (!isValid) return 'INVALID_FORMULA';
  }
  return undefined;
}

export default categoryItemValidator;
