import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import SimpleSchema from 'simpl-schema';
import categoryItemValidator from './validators/categoryItem';

SimpleSchema.extendOptions(['autoform']);

export const Category = new SimpleSchema({
  categoryName: {
    type: String,
  },
  createdAt: {
    type: Date,
    optional: true,
  },
}, { tracker: Tracker });

export const CategoryItem = new SimpleSchema({
  categoryId: {
    type: String,
  },
  name: {
    type: String,
  },
  interval: {
    type: String,
    allowedValues: ['one-off', 'monthly'],
  },
  pricingFormula: {
    type: SimpleSchema.oneOf(String, Number),
  },
}, { tracker: Tracker });

CategoryItem.messageBox.messages({
  en: {
    INVALID_FORMULA: 'formula is not valid',
  },
});

CategoryItem.addValidator(categoryItemValidator);

export const InputItem = new SimpleSchema({
  categoryId: {
    type: String,
  },
  name: {
    type: String,
  },
  value: {
    type: Number,
    optional: true,
  },
}, { tracker: Tracker });
