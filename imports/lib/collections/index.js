import { Mongo } from 'meteor/mongo';

import Schema from '../schemas';

export const Categories = new Mongo.Collection('Categories');
Categories.attachSchema(Schema.Category);

export const Inputs = new Mongo.Collection('Inputs');
Inputs.attachSchema(Schema.InputItem);

export const CategoryItems = new Mongo.Collection('CategoryItems');
Inputs.attachSchema(Schema.CategoryItems);

Categories.helpers({
  items() {},
});

CategoryItems.helpers({
  itemsWithCatgeory() {
  },
});

Inputs.helpers({
  category() {
    return Categories.findOne({ _id: this.categoryId });
  },
});
