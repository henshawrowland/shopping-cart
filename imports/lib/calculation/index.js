import exprEval from 'expr-eval';

class Calculation {
  static inputList = [];

  static getIdentifier(term, index) {
    return `x${index}`;
  }

  static getData(term) {
    const inputs = this.inputList;

    const data = inputs.find((item) => item.displayName === term.trim());
    if (data) {
      return data;
    }
    return null;
  }

  static isValidVariable(term) {
    return !!this.getData(term);
  }

  static formatWithData(formula) {
    const re = new RegExp(/\$+\{(.+?)\}/);
    const formatedFormula = [];

    const data = formula.split(re)
      .filter((e) => !!e)
      .map((term, index) => {
        let identifier = term;
        let associatedData = null;
        if (this.isValidVariable(term)) {
          identifier = this.getIdentifier(term, index);
          associatedData = {
            ...this.getData(term),
            identifier,
          };
        }
        formatedFormula.push(identifier);
        return associatedData;
      }).filter((e) => !!e);

    return [
      formatedFormula.join(''),
      data,
    ];
  }
}

Calculation.isValidFormula = function (formula, inputList) {
  Calculation.inputList = inputList;
  try {
    const [expression, uknowns] = this.formatWithData(formula);
    const parser = new exprEval.Parser();
    const parsedFormula = parser.parse(expression);
    if (parsedFormula.variables().length !== uknowns.length) {
      throw new Error('Unknown character');
    }
  } catch (e) {
    if (e) {
      return false;
    }
  }
  return true;
};

export default Calculation;
