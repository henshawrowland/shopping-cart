import { Meteor } from 'meteor/meteor';
import {
  Categories,
  Inputs,
  CategoryItems
} from '/imports/lib/collections';
import '/imports/startup/server';

Meteor.startup(() => {
  if (Categories.find().count() === 0) {
    const catId = Categories.insert({ categoryName: 'Category1', createdAt: new Date() });
    console.log('catIId', catId);
    Inputs.insert({ name:'Input1', categoryId: catId, value: 500 });
  }
});
